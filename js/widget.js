(function ($) {

  Drupal.behaviors.optionsSort = {
    attach: function (context) {

      $('.field-widget-options-sort-checkboxes', context).each(function () {

        var $widget = $(this),
          $selected = $widget.find('.field-prefix .fieldset-wrapper'),
          $checkboxes = $widget.find('.form-checkboxes');

        // Listen for changing checkboxes
        $widget.on('change', ':checkbox', function () {
          var $checkbox = $(this).closest('.form-type-checkbox');

          if (this.checked) {
            // Move this checkbox to the selected list
            $selected.append($checkbox);
          }
          else {
            // Move the checkbox back to the default list, and put it in the correct spot.

            var weight = $checkbox.find(':checkbox').data('weight');

            // Loop through all the checkboxes in the list, and find the first one
            // with a weight greater than the selected checkbox
            var $target;
            $checkboxes.find('.form-type-checkbox').each(function () {
              var $item = $(this);
              if ($item.find(':checkbox').data('weight') > weight) {
                $target = $item;
                return false;
              }
            });

            // If it found a checkbox to put it before, insert it before
            if ($target) {
              $target.before($checkbox);
            }
            // Otherwise, append selected to the bottom of the list
            else {
              $checkboxes.append($checkbox);
            }
          }

        });

        // Make list sortable
        $selected.sortable({axis: 'y'});

      });

    },
    detach: function () {
    }
  }

})(jQuery);